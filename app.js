const express = require('express')
const { success } = require('./helpers')
let pokemons = require('./mock-pockemon')
let bodyParser = require('body-parser')
let morgan = require('morgan')
const app = express()
const port = 3000



app
    .use(morgan('dev'))
    .use(bodyParser.json())




// @ts-ignore
app.get('/', (req, res) => res.send('Hello, Express!'))



app.get('/api/pokemons', (req, res) => {
    const message = 'La liste des pokémons a bien été récupérée.'
    res.json(success(message, pokemons))
})

app.get('/api/pokemons/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const pokemon = pokemons.find(pokemon => pokemon.id === id)
    const message = 'Un pokémon a bien été trouvé.'
    res.json(success(message, pokemon))
})


app.post('/api/pokemons', (req, res) => {
    const id = 123
    const pokemonCreated = {...req.body, ... { id: id, created: new Date() } }
        // @ts-ignore
    pokemons.push(pokemonCreated)
    const message = `Le pokémon ${pokemonCreated.name} a bien été crée.`
    res.json(success(message, pokemonCreated))
})


app.put('/api/pokemons/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const pokemonUpdated = {...req.body, id: id }
        // @ts-ignore
    pokemons = pokemons.map(pokemon => {
        return pokemon.id === id ? pokemonUpdated : pokemon
    })

    const message = `Le pokémon ${pokemonUpdated.name} a bien été modifié.`
    res.json(success(message, pokemonUpdated))
});

app.delete('/api/pokemons/:id', (req, res) => {
    const id = parseInt(req.params.id)
        // @ts-ignore
    const pokemonDeleted = pokemons.find(pokemon => pokemon.id === id)
        // @ts-ignore
    pokemons = pokemons.filter(pokemon => pokemon.id !== id)
    const message = `Le pokémon ${pokemonDeleted.name} a bien été supprimé.`
    res.json(success(message, pokemonDeleted))
});



app.listen(port, () => console.log(`Notre application Node est démarrée sur : http://localhost:${port}`))